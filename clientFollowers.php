<?php
 header ('Content-Type: text/html; charset=UTF-8'); 
 
 function selectAllClientDataByID($id,$id_follower){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("client");
       $selectedClient = $selectClient->getClientDataByIDFollow($id);

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedClient)){ 
         $row['followers']=selectClientFollowers($row['followers']);
          $row['isFollow']=CheckFoundFollow($id_follower,$row['isFollow']);
      
      
    $json_arr=$row;
}


    return $json_arr;
	
    
   }
  
   
   
    function selectAllClientFollower($id_client,$id_follower){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("client_follower");
       $selectedClient = $selectClient->getClientFollowers($id_client);

// $json_arr=array();
   if(mysqli_num_rows($selectedClient)>0){
while($row=mysqli_fetch_assoc($selectedClient)){
    $json_arr[]=selectAllClientDataByID($row['id_follower'],$id_follower);
}


    return json_encode($json_arr,JSON_UNESCAPED_UNICODE);;
}else{
    return "[]";
}
    
   }
    function selectClientFollowers($id_follower){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("client_follower");
       $selectedClient = $selectClient->getClientFollowDataById($id_follower);

 $json_arr;
while($row=mysqli_fetch_assoc($selectedClient)){
    $json_arr=$row['follow'];
}


    return $json_arr;
	
    
   }
    
  
#check Follow found......
function CheckFoundFollow($id_follower,$id_client)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectUsers = new Select("client_follower");
    $selectedUsers = $selectUsers->getClientFollowerData($id_follower,$id_client);
    if(mysqli_num_rows($selectedUsers)>0){
     return "true";
    }
    else{
        return "false";
    }
}
   if($_GET['id']!=''){
echo selectAllClientFollower($_GET['id'],$_GET['id_follower']);
   }
  
?>
