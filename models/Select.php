
<?php

class Select extends General
{

    public $tablename;

    public function __construct($tablename)
    {

        $this->tablename = $tablename;
        $this->connectToDB();
    }

    public function getDataWhere($username, $password)
    {
        $query = "SELECT * FROM $this->tablename WHERE username = \"$username\"";
        if ($password != null) {
            $query .= " AND password = \"$password\"";
        }
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getAllData()
    {
        # select data in tablename in connected db
        $query = "SELECT * FROM $this->tablename ORDER BY created_at DESC";

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getDataById($id)
    {
        $query = "SELECT * FROM $this->tablename WHERE id = $id ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            die($query);
            # throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getCategorysNotNull()
    {
        $query = "SELECT * FROM $this->tablename WHERE (name !='' AND name IS NOT NULL) ORDER BY orders";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            die($query);
            ##throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getCategories2($catgory1Id, $page)
    {
        $page = $page ? $page : 0;
        $limit = 20;
        $offset = $limit * $page;
        $query = "SELECT * FROM $this->tablename WHERE (name !='' AND name IS NOT NULL) AND id_category1 = $catgory1Id ORDER BY orders LIMIT $limit OFFSET $offset";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            die($query);
            ##throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getCategories3($catgory2Id, $page)
    {
        $page = $page ? $page : 0;
        $limit = 20;
        $offset = $limit * $page;
        $query = "SELECT * FROM $this->tablename WHERE (name !='' AND name IS NOT NULL) AND id_category2 = $catgory2Id ORDER BY orders LIMIT $limit OFFSET $offset";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            die($query);
            ##throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getUserDataByUserName($username)
    {
        $query = "SELECT * FROM $this->tablename WHERE username = \"$username\"";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getUserDataByEmail($username)
    {
        $query = "SELECT * FROM $this->tablename WHERE email = \"$username\"";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getProductComments($id_product)
    {
        $query = "SELECT $this->tablename.*,client.name,client.logo
 FROM $this->tablename
  INNER JOIN client ON
  $this->tablename.id_client =client.id
   AND $this->tablename.id_product =$id_product
   ORDER BY $this->tablename.created_at DESC";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getFavourateByIdClientANDProduct($id_product, $id_client)
    {
        $query = "SELECT * FROM $this->tablename WHERE id_client =$id_client AND id_product=$id_product  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getColorsDataById($id)
    {
        $query = "SELECT color FROM $this->tablename WHERE id = $id ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getCityByGovernmentId($id_govrnment)
    {
        $query = "SELECT id , name FROM $this->tablename WHERE id_govrnment = $id_govrnment ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getgovern()
    {
        $query = "SELECT * FROM $this->tablename ORDER BY orders ASC";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function updateUsersDataById($id, $email, $password)
    {
        $query = "UPDATE  $this->tablename SET `email`='$email',`password` = '$password' WHERE id = $id";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function updateClientssDataById($id, $email, $password)
    {
        $query = "UPDATE  $this->tablename SET `email`='$email',`password` = '$password' WHERE id = $id";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function updateProductViewById($id_product)
    {
        $query = "UPDATE  $this->tablename SET view =view +1,created_at =created_at WHERE id = $id_product";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getProductLovesDataById($id_product)
    {
        $query = "SELECT count(*) as love FROM $this->tablename WHERE id_product = $id_product ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getProductReviewDataById($id_client)
    {
        $query = "SELECT count(*) as review FROM $this->tablename WHERE id_client = $id_client ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getClientRateDataById($id_client)
    {
        $query = "SELECT TRUNCATE(AVG(rate),1) as stars FROM $this->tablename WHERE id_client = $id_client ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getAllDataOrderSize()
    {
        # select data in tablename in connected db
        # $query = 'SELECT * FROM movies ORDER BY title DESC';
        $query = "SELECT * FROM $this->tablename ORDER BY size_number ASC";

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getAllDataOrder()
    {
        # select data in tablename in connected db
        # $query = 'SELECT * FROM movies ORDER BY title DESC';
        $query = "SELECT * FROM $this->tablename ORDER BY orders ASC";

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getAllDataOrderName()
    {
        # select data in tablename in connected db
        # $query = 'SELECT * FROM movies ORDER BY title DESC';
        $query = "SELECT * FROM $this->tablename ORDER BY orders ASC, name ASC";

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getProductLimit($limit)
    {
        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
   ORDER BY $this->tablename.id DESC
  LIMIT $limit, 20";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getProductClientLoveLimit($limit, $id_client)
    {
        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  INNER JOIN love ON
  $this->tablename.id =love.id_product
  AND love.id_client=$id_client
   ORDER BY $this->tablename.id DESC
  LIMIT $limit, 20";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getProductDataByID($id)
    {
        $query = "SELECT $this->tablename.description,$this->tablename.title ,
  $this->tablename.id as share, $this->tablename.id_category1,$this->tablename.id_category2,$this->tablename.id_category3,
 $this->tablename.img1,$this->tablename.swap,$this->tablename.img2,$this->tablename.img3,
 $this->tablename.img4,$this->tablename.img5,$this->tablename.id_brand,
 $this->tablename.id_color1 as color1, $this->tablename.id_color2 as color2,
 $this->tablename.price, $this->tablename.swap,$this->tablename.view, $this->tablename.created_at,
 $this->tablename.id_client , $this->tablename.id as love,$this->tablename.id as isLove,$this->tablename.id_client as review,
 $this->tablename.id_client as stars,
 $this->tablename.id as comment
 ,brands.name as brand ,size.name as size ,size.size_number,condition_state.title as state ,government.name as government,city.name as city,
 client.name,client.logo ,client.phone
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN condition_state ON
  $this->tablename.id_condition_state = condition_state.id
  INNER JOIN government ON
  $this->tablename.id_government = government.id
  INNER JOIN city ON
  $this->tablename.id_city = city.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  AND $this->tablename.id =$id
  ";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #all products By client Id
    public function getProductLimitByIDClient($limit, $id)
    {
        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  AND $this->tablename.id_client =$id
   ORDER BY $this->tablename.id DESC
  LIMIT $limit, 20";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #all products by category3 id
    public function getProductsByCategory3($category3Id, $page)
    {
        $page = $page ? $page : 0;
        $limit = 20;
        $offset = $limit * $page;

        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  AND $this->tablename.id_category3 =$category3Id
  ORDER BY $this->tablename.id DESC
  LIMIT $limit OFFSET $offset";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #all products by brand id
    public function getProductLimitByIDBrand($limit, $id_category1, $id_category2, $id_category3)
    {
        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  AND $this->tablename.id_category1 =$id_category1
  AND $this->tablename.id_category2 =$id_category2
  AND $this->tablename.id_category3 =$id_category3
  ORDER BY $this->tablename.id DESC
  LIMIT $limit, 20";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #edit product data
    public function getProductDataByIDEdit($id)
    {
        $query = "SELECT $this->tablename.description,$this->tablename.title ,
   $this->tablename.id_category1,category1.name as category1,
  $this->tablename.id_category2,category2.name as category2,
  $this->tablename.id_category3,category3.name as category3 ,
 $this->tablename.img1,$this->tablename.swap,$this->tablename.img2,$this->tablename.img3,
 $this->tablename.img4,$this->tablename.img5,
 $this->tablename.id_color1,$this->tablename.id_color1 as color1,
 $this->tablename.id_color2, $this->tablename.id_color2 as color2,
 $this->tablename.price, $this->tablename.swap,$this->tablename.view, $this->tablename.created_at,
 $this->tablename.id_client ,
  $this->tablename.id_brand,brands.name as brand ,
   $this->tablename.id_size,size.name as size ,size.size_number,
 $this->tablename.id_condition_state,condition_state.title as state ,
  $this->tablename.id_government,government.name as government,
  $this->tablename.id_city, city.name as city
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN condition_state ON
  $this->tablename.id_condition_state = condition_state.id
  INNER JOIN government ON
  $this->tablename.id_government = government.id
  INNER JOIN city ON
  $this->tablename.id_city = city.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
   INNER JOIN category1 ON
  $this->tablename.id_category1 =category1.id
   INNER JOIN category2 ON
  $this->tablename.id_category2 =category2.id
   INNER JOIN category3 ON
  $this->tablename.id_category3 =category3.id
  AND $this->tablename.id =$id
  ";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client Data By Id
    public function getClientDataByID($id)
    {
        $query = "SELECT $this->tablename.name,$this->tablename.about ,$this->tablename.phone,

 $this->tablename.logo , $this->tablename.id as follow,$this->tablename.id as followers,$this->tablename.id as review,
 $this->tablename.id as stars, $this->tablename.id as isFollow
 FROM $this->tablename
WHERE $this->tablename.id =$id
 ORDER BY $this->tablename.id DESC
  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client follower
    public function getClientFollowDataById($id_client)
    {
        $query = "SELECT count(*) as follow FROM $this->tablename WHERE id_client = $id_client ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client followers
    public function getClientFollowersDataById($id_client)
    {
        $query = "SELECT count(*) as followers FROM $this->tablename WHERE id_follower = $id_client ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client follower
    public function getClientFollowerData($id_follower, $id_client)
    {
        $query = "SELECT * FROM $this->tablename WHERE id_client =$id_client AND id_follower=$id_follower ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client Review data
    public function getDataByClientID($id_client)
    {
        $query = "SELECT * FROM $this->tablename WHERE id_client = $id_client  ORDER BY $this->tablename.created_at DESC";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client following

    public function getClientFollowing($id_client)
    {
        $query = "SELECT * FROM $this->tablename WHERE id_follower = $id_client  ORDER BY $this->tablename.created_at DESC ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client following

    public function getClientFollowers($id_client)
    {
        $query = "SELECT * FROM $this->tablename WHERE id_client = $id_client  ORDER BY $this->tablename.created_at DESC ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getClientDataByIDFollow($id)
    {
        $query = "SELECT $this->tablename.id, $this->tablename.name,

 $this->tablename.logo ,$this->tablename.id as followers,
 $this->tablename.id as isFollow
 FROM $this->tablename
WHERE $this->tablename.id =$id  ORDER BY $this->tablename.id DESC
  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getClientDataInReview($id)
    {
        $query = "SELECT $this->tablename.id,$this->tablename.rate ,$this->tablename.data,

 $this->tablename.id_rate_client ,client.name,client.logo,$this->tablename.created_at
 FROM $this->tablename
 INNER JOIN client ON
  $this->tablename.id_rate_client =client.id
WHERE $this->tablename.id_client =$id  ORDER BY $this->tablename.id DESC
  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #client product data
    #all products By client Id
    public function getProductClientData($id)
    {
        $query = "SELECT $this->tablename.id_client,$this->tablename.title ,client.username,client.logo
 FROM $this->tablename
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  AND $this->tablename.id =$id
  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getClientDataInMessage($id_sent, $id_recieve)
    {
        $query = "SELECT $this->tablename.id, $this->tablename.message ,$this->tablename.created_at,$this->tablename.id_sent
 ,$this->tablename.seen,$this->tablename.id_product
 FROM $this->tablename
WHERE ($this->tablename.id_sent =$id_sent AND $this->tablename.id_recieve =$id_recieve)
OR ($this->tablename.id_sent =$id_recieve AND $this->tablename.id_recieve =$id_sent) ORDER BY $this->tablename.id ASC
  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getClientDataInMessageByProduct($id_sent, $id_recieve, $id_product)
    {
        $query = "SELECT $this->tablename.id, $this->tablename.message ,$this->tablename.created_at,$this->tablename.id_sent
 ,$this->tablename.seen,$this->tablename.id_product
 FROM $this->tablename
WHERE ($this->tablename.id_sent =$id_sent AND $this->tablename.id_recieve =$id_recieve AND $this->tablename.id_product = $id_product)
OR ($this->tablename.id_sent =$id_recieve AND $this->tablename.id_recieve =$id_sent AND $this->tablename.id_product = $id_product) ORDER BY $this->tablename.id ASC
  ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getClientDataInChat($id)
    {
        $query = "SELECT  DISTINCT  case when $this->tablename.id_sent = $id then $this->tablename.id_recieve
            when $this->tablename.id_recieve = $id then  $this->tablename.id_sent end as id
            FROM $this->tablename  ORDER BY $this->tablename.id DESC ";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client
    public function getCountUnReedMessage($id, $id_sent)
    {
        $query = "SELECT count(*) as unreed FROM $this->tablename WHERE $this->tablename.id_recieve =$id AND $this->tablename.id_sent=$id_sent AND seen=0";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    #client
    public function getCountUnReedMessageRecieve($id_client)
    {
        $query = "SELECT count(*) as unreed FROM $this->tablename WHERE id_recieve = $id_client AND seen=0";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #search
    public function getProductLimitBySearch($limit, $text)
    {
        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
  AND $this->tablename.title LIKE '%$text%'
   ORDER BY $this->tablename.created_at DESC
  LIMIT $limit, 20";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }
    public function getProductLimitBySearchALL($limit, $newQuery, $price_from, $price_to)
    {
        $query = "SELECT $this->tablename.id ,$this->tablename.img1,$this->tablename.swap,$this->tablename.price,$this->tablename.id_client , $this->tablename.id as love
 ,$this->tablename.id as isLove,brands.name as brand ,size.name as size ,size.size_number,client.username,client.logo
 FROM $this->tablename INNER JOIN brands ON
  $this->tablename.id_brand =brands.id
  INNER JOIN size ON
  $this->tablename.id_size =size.id
  INNER JOIN client ON
  $this->tablename.id_client =client.id
 $newQuery

   ORDER BY $this->tablename.created_at DESC
  LIMIT $limit, 20";

        // var_dump($query);
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        // var_dump($sql);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #forms
    public function getAllDataForms($id_kind)
    {
        # select data in tablename in connected db
        if ($id_kind != -1) {
            $query = "SELECT $this->tablename.id ,$this->tablename.title,$this->tablename.content,form_kind.name as kind
             FROM $this->tablename
             INNER JOIN form_kind ON
  $this->tablename.id_kind =form_kind.id
   AND $this->tablename.id_kind = $id_kind
             ORDER BY $this->tablename.created_at DESC";
        } else {
            $query = "SELECT $this->tablename.id ,$this->tablename.title,$this->tablename.content,form_kind.name as kind
             FROM $this->tablename
              INNER JOIN form_kind ON
  $this->tablename.id_kind =form_kind.id
             ORDER BY $this->tablename.created_at DESC";
        }

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getAllDataFormsById($id)
    {
        # select data in tablename in connected db
        $query = "SELECT $this->tablename.id ,$this->tablename.title,$this->tablename.content,$this->tablename.img1,$this->tablename.swap,$this->tablename.img2,$this->tablename.img3,
            $this->tablename.img4 ,$this->tablename.img5,form_kind.name as kind,$this->tablename.created_at ,$this->tablename.id as countComments
            FROM $this->tablename
             INNER JOIN form_kind ON
  $this->tablename.id_kind =form_kind.id
             WHERE $this->tablename.id=$id ORDER BY $this->tablename.created_at DESC";

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #count comment form
    public function getCountCommentForm($id)
    {
        $query = "SELECT count(*) as comment FROM $this->tablename WHERE id_form = $id ";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #comments forms
    public function getFormComments($id_form)
    {
        $query = "SELECT $this->tablename.*,client.name,client.logo
 FROM $this->tablename
  INNER JOIN client ON
  $this->tablename.id_client =client.id
   AND $this->tablename.id_form =$id_form
   ORDER BY $this->tablename.created_at DESC";

        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    #forms Kind
    public function getAllDataFormsKind()
    {
        # select data in tablename in connected db
        $query = "SELECT * FROM $this->tablename ORDER BY id DESC";

        # if the select query succeded will return selected data else will throw exception
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function getPasswordReset($token)
    {
        $query = "SELECT * FROM $this->tablename WHERE token = '$token'";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);
        if (!$sql) {
            die($query);
            # throw new \Exception("Error : Can not display data ");
        } else {
            return $sql;
        }
    }

    public function updatePassword($hashed_password, $email)
    {
        $query = "UPDATE $this->tablename SET password = '$hashed_password' WHERE email = '$email'";
        # if the select query succeded will return selected data else will throw exception7
        $sql = mysqli_query($this->cxn->handle, $query);

        if (!$sql) {
            die($query);
            # throw new \Exception("Error : Can not display data ");
        } else {
            return TRUE;
        }
    }
}
