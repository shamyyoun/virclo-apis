<?php
 header ('Content-Type: text/html; charset=UTF-8'); 
 
 function selectAllClientDataByID($id,$id_follower){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("client");
       $selectedClient = $selectClient->getClientDataByID($id);

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedClient)){

      $row['review']=selectAllClientReview($row['review']);
        $row['follow']=selectClientFollowers($row['follow']);
         $row['followers']=selectAllClientFollower($row['followers']);
          $row['isFollow']=CheckFoundFollow($id_follower,$row['isFollow']);
       $row['stars']=selectAllClientStars($row['stars']);
      
    $json_arr=$row;
}


    return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
  
   
    function selectAllClientReview($id_client){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("review");
       $selectedClient = $selectClient->getProductReviewDataById($id_client);

 $json_arr;
while($row=mysqli_fetch_assoc($selectedClient)){
    $json_arr=$row['review'];
}


    return $json_arr;
	
    
   }
    function selectAllClientFollower($id_client){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("client_follower");
       $selectedClient = $selectClient->getClientFollowDataById($id_client);

 $json_arr;
while($row=mysqli_fetch_assoc($selectedClient)){
    $json_arr=$row['follow'];
}


    return $json_arr;
	
    
   }
    function selectClientFollowers($id_follower){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("client_follower");
       $selectedClient = $selectClient->getClientFollowersDataById($id_follower);

 $json_arr;
while($row=mysqli_fetch_assoc($selectedClient)){
    $json_arr=$row['followers'];
}


    return $json_arr;
	
    
   }
    function selectAllClientStars($id_client){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectClient = new Select("review");
       $selectedClient = $selectClient->getClientRateDataById($id_client);

 $json_arr;
while($row=mysqli_fetch_assoc($selectedClient)){
    $json_arr=$row['stars'] ;
}


    return $json_arr;
	
    
   }
  
#check Follow found......
function CheckFoundFollow($id_follower,$id_client)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectUsers = new Select("client_follower");
    $selectedUsers = $selectUsers->getClientFollowerData($id_follower,$id_client);
    if(mysqli_num_rows($selectedUsers)>0){
     return "true";
    }
    else{
        return "false";
    }
}
   if($_GET['id']!=''){
echo selectAllClientDataByID($_GET['id'],$_GET['id_follower']);
   }
  
?>
