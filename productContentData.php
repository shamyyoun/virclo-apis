<?php
 header ('Content-Type: text/html; charset=UTF-8'); 

 function selectAllBrands(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("brands");
       $selectedMainServices = $selectMainServices->getAllDataOrderName();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"brands");

     return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
function selectAllColor(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("color");
       $selectedMainServices = $selectMainServices->getAllDataOrder();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"color");

      return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
   function selectAllCategory1(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("category1");
       $selectedMainServices = $selectMainServices->getCategorysNotNull();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"category1");

    return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
 function selectAllCategory2(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("category2");
       $selectedMainServices = $selectMainServices->getCategorysNotNull();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"category2");

     return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
 function selectAllCategory3(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("category3");
       $selectedMainServices = $selectMainServices->getCategorysNotNull();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"category3");

     return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
     function selectAllSize(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("size");
       $selectedMainServices = $selectMainServices->getAllDataOrderSize();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"size");

      return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
     function selectAllCondition(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("condition_state");
       $selectedMainServices = $selectMainServices->getAllDataOrder();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"condition_state");

      return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
     function selectAllGovernment(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("government");
       $selectedMainServices = $selectMainServices->getAllDataOrderName();

 $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

$arr=array("table" =>"government");

      return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
	
    
   }
     function selectAllGovernments(){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";

       require_once $fullGeneralPath;
       require_once $fullSelectPath;

       $selectMainServices = new Select("government");
       $selectedMainServices = $selectMainServices->getgovern();

 

    return $selectedMainServices;
	
    
   }

  function selectAllCityByIdGovern($id_govrnment){
     //  $dir =  dirname(dirname(__FILE__));
       $fullGeneralPath = "models/General.php";
       $fullSelectPath = "models/Select.php";
       require_once $fullGeneralPath;
       require_once $fullSelectPath;
       $selectMainServices = new Select("city");
       $selectedMainServices = $selectMainServices->getCityByGovernmentId($id_govrnment);
  
     $json_arr=array();
while($row=mysqli_fetch_assoc($selectedMainServices)){
    $json_arr[]=$row;
}

      return json_encode($json_arr,JSON_UNESCAPED_UNICODE);
    
   }
   
   echo '{"catregory1":'.selectAllCategory1().",";
   echo'"catregory2":'.selectAllCategory2().",";
   echo '"catregory3":'.selectAllCategory3().",";
   echo '"brands":'.selectAllBrands().",";
  echo '"color":'.selectAllColor().",";
  echo '"condition":'.selectAllCondition().",";
  echo '"size":'.selectAllSize().",";
  echo '"government":'.selectAllGovernment().",";
 echo '"city":{';
 $count=count(selectAllGovernments());
 $i=0;
  foreach(selectAllGovernments() as $govern){
      if($i !=0) echo ',';
      $i++;
    
           echo '"'.$govern['id'].'":'.selectAllCityByIdGovern($govern['id']);
    
     
    

  }
  echo '}}';


 
?>
