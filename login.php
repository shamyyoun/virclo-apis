<?php
header('Content-Type: text/html; charset=UTF-8');
function logincheck($username, $password)
{
    //  $dir =  dirname(dirname(__FILE__));
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectClient = new Select("client");
    $selectedClients = $selectClient->getDataWhere($username, null);
    if (mysqli_num_rows($selectedClients) > 0) {
        $json_arr = array();
        while ($row = mysqli_fetch_assoc($selectedClients)) {
            $json_arr = $row;
        }

        // verify password
        if (!password_verify($password, $json_arr["password"])) {
            return false;
        }

        $arr = array("sucess" => "true", "countUnSeen" => selectAllClientUnReed($json_arr['id']));
        $json_arr = $arr + $json_arr;
        return json_encode($json_arr, JSON_UNESCAPED_UNICODE);
    } else {
        return false;
    }

}
function selectAllClientUnReed($id_client)
{
    //  $dir =  dirname(dirname(__FILE__));
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectProduct = new Select("messages");
    $selectedProduct = $selectProduct->getCountUnReedMessageRecieve($id_client);

    $json_arr;
    while ($row = mysqli_fetch_assoc($selectedProduct)) {
        $json_arr = $row['unreed'];
    }

    return $json_arr;

}

#login an Client data ..
if (isset($_GET['username'])) {

    $username = $_GET['username'];
    $password = $_GET['password'];

    if (logincheck($username, $password) != false) {
        echo logincheck($username, $password);
    } else {
        echo '{"sucess":false}';
    }
}
