<?php
header('Content-Type: application/json; charset=UTF-8');

function selectData()
{
    if (!isset($_GET['category2_id'])) {
        throw new Exception("Error: missing params");
        return;
    }

    require_once "models/General.php";
    require_once "models/Select.php";

    $selectData = new Select("category3");
    $selectedData = $selectData->getCategories3($_GET['category2_id'], $_GET['page']);

    $data = array();
    while ($row = mysqli_fetch_assoc($selectedData)) {
        $data[] = $row;
    }

    return json_encode($data, JSON_UNESCAPED_UNICODE);
}

echo selectData();