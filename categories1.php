<?php
header('Content-Type: application/json; charset=UTF-8');
function selectData()
{
    require_once "models/General.php";
    require_once "models/Select.php";

    $selectData = new Select("category1");
    $selectedData = $selectData->getCategorysNotNull();

    $data = array();
    while ($row = mysqli_fetch_assoc($selectedData)) {
        $data[] = $row;
    }

    return json_encode($data, JSON_UNESCAPED_UNICODE);
}

echo selectData();
