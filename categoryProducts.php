<?php
header('Content-Type: application/json; charset=UTF-8');

require_once "models/General.php";
require_once "models/Select.php";

function selectData()
{
    if (!isset($_GET['category3_id']) || !isset($_GET['user_id'])) {
        throw new Exception("Error: missing params");
        return;
    }

    $selectData = new Select("product");
    $selectedData = $selectData->getProductsByCategory3($_GET['category3_id'], $_GET['page']);

    $json_arr = array();
    while ($row = mysqli_fetch_assoc($selectedData)) {
        $row['love'] = selectAllProductLove($row['love']);
        $row['isLove'] = checkFoundLove($row['isLove'], $_GET['user_id']);
        $json_arr[] = $row;
    }

    return json_encode($json_arr, JSON_UNESCAPED_UNICODE);
}

function selectAllProductLove($productId)
{
    $selectData = new Select("love");
    $selectedData = $selectData->getProductLovesDataById($productId);

    $json_arr;
    while ($row = mysqli_fetch_assoc($selectedData)) {
        $json_arr = $row['love'];
    }

    return $json_arr;
}

function checkFoundLove($productId, $userId)
{
    $selectData = new Select("love");
    $selectedData = $selectData->getFavourateByIdClientANDProduct($productId, $userId);
    if (mysqli_num_rows($selectedData) > 0) {
        return "true";
    } else {
        return "false";
    }
}

echo selectData();