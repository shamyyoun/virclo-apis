<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Virclo | Reset Password</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>

<?php

// require required files
require_once "models/General.php";
require_once "models/Select.php";

// define functions
function getPasswordReset()
{
    $selectData = new Select("password_resets");
    $selectedData = $selectData->getPasswordReset($_GET['token']);
    if (mysqli_num_rows($selectedData) > 0) {
        while ($row = mysqli_fetch_assoc($selectedData)) {
            return $row;
        }
    } else {
        return null;
    }
}

function isValidPasswordReset($passwordReset) {
    if ($passwordReset) {
        $expire = strtotime($passwordReset['created_at'] . ' + 1 days');
        $today = strtotime("today midnight");

        return $today < $expire;
    } else {
        return false;
    }
}

function hasChangePasswordParams() {
    return isset($_POST["new_password"]) && isset($_POST["re_new_password"]);
}

function changePassword($email) {
    $newPassword = $_POST["new_password"];
    $reNewPassword = $_POST["re_new_password"];

    if (empty(trim($newPassword))) {
        return 2;
    }

    if ($newPassword != $reNewPassword) {
        return 3;
    }

    $hashedPassword = password_hash($newPassword, PASSWORD_BCRYPT);

    $updateData = new Select("client");
    $updateData = $updateData->updatePassword($hashedPassword, $email);

    if ($updateData) {
        return 1;
    } else {
        return 0;
    }
}

$mainHtml =
'<div class="col-md-4 col-md-offset-4">
<div class="panel panel-default">
<div class="panel-body">
    <div class="text-center">
    <h3><i class="fa fa-lock fa-4x"></i></h3>
    <h2 class="text-center">Reset Password</h2>
    <div class="panel-body">

        <form id="register-form" role="form" autocomplete="off" class="form" method="post">

        <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="password" name="new_password" placeholder="Enter new password..." class="form-control"  type="password">
            </div>

            <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="password" name="re_new_password" placeholder="Re-enter password..." class="form-control"  type="password">
            </div>
        </div>
        <div class="form-group">
            <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit">
        </div>
        </form>

    </div>
    </div>
</div>
</div>
</div>';

$invalidTokenErrorHtml =
'<div class="col-md-4 col-md-offset-4 text-center">
    <h2 style="color: red;">Invalid or expired link.</h2> 
    <h4 style="color: gray;">Please try reset password again.</h4> 
</div>';

$failedChangingPasswordErrorHtml =
'<div class="col-md-4 col-md-offset-4 text-center">
    <h2 style="color: red;">Error updaing password.</h2> 
    <h4 style="color: gray;">Please try again or contact administrator.</h4> 
</div>';

$enterPasswordsErrorHtml =
'<div class="col-md-4 col-md-offset-4 text-center">
    <h2 style="color: red;">Please enter the new password.</h2> 
</div>';

$passwordsDoesntMatchPasswordErrorHtml =
'<div class="col-md-4 col-md-offset-4 text-center">
    <h2 style="color: red;">Passwords does not match.</h2> 
</div>';

$changingPasswordSuccessHtml =
'<div class="col-md-4 col-md-offset-4 text-center">
    <h2 style="color: green;">Password changed successfully.</h2> 
    <h4 style="color: gray;">You can now use the now password in the app to login.</h4> 
</div>';
?>

    <div class="container" style="padding-top: 70px;">
        <div class="row">
            <?php
            // check password reset
            $passwordReset = getPasswordReset();
            if (!isValidPasswordReset($passwordReset)) {
                // show error
                echo $invalidTokenErrorHtml;
            } else {
                if (hasChangePasswordParams()) {
                    // try to change password
                    $changeResult = changePassword($passwordReset['email']);
                    if ($changeResult == 1) {
                        // show success
                        echo $changingPasswordSuccessHtml;
                    } else {
                         // show error
                         if ($changeResult == 2) {
                            echo $enterPasswordsErrorHtml;
                         } else if ($changeResult == 3) {
                            echo $passwordsDoesntMatchPasswordErrorHtml;
                         } else {
                            echo $failedChangingPasswordErrorHtml;
                         }
                         
                         // show main html
                         echo $mainHtml;
                    }
                } else {
                    // show main html
                    echo $mainHtml;
                }
            }
            ?>
        </div>
    </div>
</body>

</html>
