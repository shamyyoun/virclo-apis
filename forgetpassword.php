<?php
header('Content-Type: text/html; charset=UTF-8');
require_once "lib/random_compat-2.0.18/lib/random.php";

if ($_GET['email'] != '') {
#check and add users....
    if (CheckFound($_GET['email']) != false) {
        echo '{"sucess":true}';
    } else {
        echo '{"sucess":false}';
    }
}

#check client found......
function CheckFound($email)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectUsers = new Select("client");
    $selectedUsers = $selectUsers->getUserDataByEmail($email);
    if (mysqli_num_rows($selectedUsers) > 0) {
        while ($row = mysqli_fetch_assoc($selectedUsers)) {
            return addPasswordResetsRecord($row['name'], $row['email']);
        }
    } else {
        return false;
    }
}

function addPasswordResetsRecord($name, $email)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Insert.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $data['email'] = $email;
    $data['token'] = bin2hex(random_bytes(15));
    $insertData = new Insert($data, "password_resets");
    $insertedData = $insertData->addData();

    if ($insertedData) {
        return sendEmail($name, $email, $data['token']);
    } else {
        return false;
    }
}

function sendEmail($name, $email, $token)
{
    require_once "emailTemplates.php";

    $to = $email;
    $from = 'Virclo <no-reply@virclo.net>';

    $subject = 'Reset Password';

    $headers = "From: " . $from . "\r\n";
    $headers .= "Reply-To: " . $from . "\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


    $resetUrl = $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']) . "/resetPassword.php?token=" . $token;
    $message = getResetPasswordTemplate($name, $resetUrl);

    $success = @mail($to, $subject, $message, $headers);
    return $success;
}
