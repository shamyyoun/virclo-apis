<?php

#insert an Client data ..
if (isset($_GET['username'])) {

    $data['username'] = $_GET['username'];
    $data['password'] = password_hash($_GET['password'], PASSWORD_BCRYPT);
    $data['state'] = 1;

    if($_GET['email'] != 'facebook' && $_GET['email'] != 'google') {
        $data['email'] = $_GET['email'];
    }

#check and add users....
    if ($data['email'] != '' && CheckFound($data['email']) != false) {
        $data['id_user'] = CheckFound($data['email']);
    } else {
        $userData['email'] = $data['email'];
        $userData['username'] = $data['username'];
        $userData['password'] = $data['password'];
        $data['id_user'] = insertIntoUsers($userData);
    }

    if (isset($data['email'])) {
        $checkEmailResult = CheckFoundByEmail($data['email']);
    } else {
        $checkEmailResult = false;
    }
    
    $checkUsernameResult = CheckUserFound($data['username']);

    if ($checkEmailResult || $checkEmailResult) {
        //  $deletedClient=deleteDataOfOneClient($data['username']);
        // $addedClient = insertIntoClient($data);

        $addedClient = 0;
    } else {
        $addedClient = insertIntoClient($data);
    }

    if ($addedClient == 1) {
        echo '{"sucess":true}';
    } else {
        $msg = '';
        if ($checkUsernameResult) {
            $msg = 'Username already exists.';
        }
        if ($checkEmailResult) {
            if (!empty($msg)) {
                $msg .= " ";
            }
            $msg .= 'Email address already exists.';
        }
        echo '{"sucess":false, "msg": "'.$msg.'"}';
    }
}
function insertIntoClient($data)
{
    //  $dir =  dirname(dirname(__FILE__));
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Insert.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $insertClient = new Insert($data, "client");
    $insertedClient = $insertClient->addData();
    return $insertedClient;
}
function insertIntoUsers($data)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Insert.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $insertOrder = new Insert($data, "users");
    $insertedOrder = $insertOrder->addDataAndGetLast();

    return $insertedOrder;
}
#check client found......
function CheckUserFound($username)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectUsers = new Select("client");
    $selectedUsers = $selectUsers->getUserDataByUserName($username);
    if (mysqli_num_rows($selectedUsers) > 0) {
        return true;
    } else {
        return false;
    }
}
function CheckFoundByEmail($email)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectUsers = new Select("client");
    $selectedUsers = $selectUsers->getUserDataByEmail($email);
    if (mysqli_num_rows($selectedUsers) > 0) {
        return true;
    } else {
        return false;
    }
}
#check client found......
function CheckFound($username)
{
    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Select.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $selectUsers = new Select("users");
    $selectedUsers = $selectUsers->getUserDataByEmail($username);
    if (mysqli_num_rows($selectedUsers) > 0) {
        while ($row = mysqli_fetch_assoc($selectedUsers)) {
            return $row["id"];
        }
    } else {
        return false;
    }
}
#delete user if found...
function deleteDataOfOneClient($username)
{

    $fullGeneralPath = "models/General.php";
    $fullSelectPath = "models/Delete.php";

    require_once $fullGeneralPath;
    require_once $fullSelectPath;

    $deleteClient = new Delete("client", "WHERE `username` = \"$username\" ");
    $deletedClient = $deleteClient->deleteData();
    return $deletedClient;
}
